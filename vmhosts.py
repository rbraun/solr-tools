import requests
import re
from enum import Enum
from dotenv import dotenv_values

from inventory import Environment
from lxml import etree
import pandas as pd
from dataclasses import dataclass
import math


@dataclass
class VmHost:
    ESX_ID_REGEX = re.compile(r'.*vmhost(\S+?)(?:\s|192\.).*')

    name: str
    esx_host: str

    def get_esx_id(self):
        match = self.ESX_ID_REGEX.fullmatch(self.esx_host)
        return match.group(1) if match else self.esx_host


class VmHostInventory:
    VM_INVENTORY_URL = "http://192.168.99.220:5000/"

    # Ren may add a hostname column to the tool at some point, for the moment need to approximate it
    # from original vm label which can be funky
    RESOLVED_NAMES = {
        "slr-liv-cld02": "slr-liv-cld02-16.04"
    }

    def __init__(self, env):
        config = dotenv_values(".env")
        self.vm_hosts = self._load_inventory_from_coreops(env.value)

    def get_hosts(self):
        return self.vm_hosts

    def get_host(self, server):
        # shorten to hostname if not already
        name = server.split('.')[0]

        # some cleaning cruft in here, the vm host tool is a WIP and needs some cleaning and extensions so
        # this is just cobbled together for the moment
        clean_prefix = self.RESOLVED_NAMES[name] if name in self.RESOLVED_NAMES else name

        for host in self.vm_hosts:
            if host.name.startswith(clean_prefix):
                return host
        return None

    def _load_inventory_from_coreops(self, env):
        tables = pd.read_html(self.VM_INVENTORY_URL)  # Returns list of all tables on page
        vm_host_table = tables[0]  # Select table of interest

        return [VmHost(row.Name, row.Esx) for row in vm_host_table.itertuples()
                if str(row.Environment) == str(math.nan) or row.Environment == env]


if __name__ == "__main__":
    # env = Environment(input(f"Environment {[e.value for e in Environment]}: "))
    env = Environment.STAGE

    vm_inventory = VmHostInventory(env)
    vm_hosts = vm_inventory.get_hosts()
    print(vm_hosts)
