from solr.collections_client import CollectionsClient
from solr.exceptions import ReplicaAlreadyExists

new_replicas = [
    'UGC-SHELF-WA-SEATTLE',
]

receiver = 'slr-stg-cld01.bcommons.net'

node_client = CollectionsClient(receiver)

for replica in new_replicas:
    try:
        node_client.add_replica(replica)
        print()
    except ReplicaAlreadyExists:
        print("Skipping existing collection " + replica + ".")
