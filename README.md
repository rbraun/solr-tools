# README #

These scripts are some preliminary tools for working with solrcloud collections.
They are not production-ready and need to be cleaned up.  Use at your own risk.

## Installation

1. Clone git clone git@bitbucket.org:rbraun/solr-tools.git.
1. Open solr-tools in PyCharm.
1. Setup interpreter to python3.
