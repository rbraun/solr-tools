from environment import Environment
from solr.network import SolrNetwork

network = SolrNetwork(Environment.STAGE)
for collection, tuples in network.collections_map.items():
    print(str(collection) + " -> " + str([host[0] for host in tuples]))
