# This is quick and dirty scripting to generate some experimental graphs and cutoff values and is
# likely buggy, so don't use for anything important!
# See https://bibliocommons.atlassian.net/wiki/spaces/SEARCH/pages/820576722/Search+Relevance+Experiment+11-2019.

import urllib
import json
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import math


def is_int(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


def generate_aliases(raw_aliases):
    valid_aliases = {}
    collection_aliases = {}
    for key in raw_aliases.keys():
        if key == 'UGC_CONTENT':
            continue
        clean_key = key
        value = raw_aliases[clean_key]
        if value == 'DEV_NULL' or value == 'UGC-BIB':
            continue
        if clean_key.startswith('core'):
            clean_key = key[4:]
        if not is_int(clean_key):
            collection_aliases[clean_key] = value
            continue
        valid_aliases[value] = clean_key

    return valid_aliases


collections = [
    "ALB-STALBERT",
    "OH-CLC"
]

url = "http://solrcloud008.core.prod.bf1.corp.pvt:8983/solr/admin/collections?action=CLUSTERSTATUS&wt=json"
response = urllib.request.urlopen(url)
data = json.loads(response.read())
all_collections = data['cluster']['collections'].keys()
aliases = generate_aliases(data['cluster']['aliases'])

cutoffs = {}

for collection in all_collections:
    url = "http://solrcloud008.core.prod.bf1.corp.pvt:8983/solr/" + collection + \
          "/select?facet.field=lib_popularity_f_circ&facet.limit=1000000&facet.mincount=1&" \
          "facet=true&indent=on&q=*:*&wt=json&rows=0"
    response = urllib.request.urlopen(url)
    data = json.loads(response.read())
    facet = data['facet_counts']['facet_fields']['lib_popularity_f_circ']
    if not facet:
        continue

    ### original densities

    pops = np.round(np.array(facet[::2]).astype(np.float), 3)
    counts = np.array(facet[1::2]).astype(np.int)

    # get rid of 0 popularity
    result = np.where(pops == 0)
    pops = np.delete(pops, result)
    pop0 = counts[result][0] if len(counts[result]) > 0 else 0
    counts = np.delete(counts, result)

    numDocs = data['response']['numFound']

    pop_range = (0.001, 100)
    bins = 100
    density = True

    hist = np.histogram(pops, weights=counts, range=pop_range, bins=bins, density=density)

    print(collection)
    print(hist)
    densities = hist[0]
    max_pop_value = (next(i for i in reversed(range(len(densities))) if densities[i] != 0)) + 1
    max_density = max(densities)
    lte1 = round(sum(densities[:1]), 4)
    lte5 = round(sum(densities[:5]), 4)
    lte10 = round(sum(densities[:10]), 4)
    lte20 = round(sum(densities[:20]), 4)
    lte50 = round(sum(densities[:50]), 4)
    lte80 = round(sum(densities[:80]), 4)
    gt80 = round(sum(densities[81:101]), 4)

    cutoff = 0
    total = 0
    cutoff_density = 0.9999
    for i in range(len(densities)):
        total += densities[i]
        if total >= cutoff_density:
            break
        cutoff = i

    if collection in aliases:
        cutoffs[aliases[collection]] = cutoff

    plt.hist(pops, weights=counts, range=pop_range, bins=bins, density=density)
    plt.title(collection)
    stats = "# docs = " + str(numDocs) + "\n\n" \
            + "pop(0) = " + str(pop0) \
            + " (" + str(math.floor(round(100 * pop0 / numDocs, 0))) + "%)\n" \
            + "max(pop) = " + str(max_pop_value) + "\n\n" \
            + "%.4f" % lte1 + " <= pop(1)" + "\n" \
            + "%.4f" % lte5 + " <= pop(5)" + "\n" \
            + "%.4f" % lte10 + " <= pop(10)" + "\n" \
            + "%.4f" % lte20 + " <= pop(20)" + "\n" \
            + "%.4f" % lte50 + " <= pop(50)" + "\n" \
            + "%.4f" % lte80 + " <= pop(80)" + "\n" \
            + "%.4f" % gt80 + " >  pop(80)" + "\n\n" \
            + "pop(>=0.9990) = " + str(cutoff)

    plt.text(60, round(max_density, 1) - 0.05, stats, horizontalalignment='left', verticalalignment='top')
    plt.savefig("popdensity/" + collection + ".png")
    plt.close()

    ### revised densities
    pops = np.minimum(pops, np.full(len(pops), fill_value=cutoff)) * 100 / cutoff
    hist = np.histogram(pops, weights=counts, range=pop_range, bins=bins, density=density)

    print(collection)
    print(hist)
    densities = hist[0]
    max_pop_value = (next(i for i in reversed(range(len(densities))) if densities[i] != 0)) + 1
    max_density = max(densities)
    lte1 = round(sum(densities[:1]), 4)
    lte5 = round(sum(densities[:5]), 4)
    lte10 = round(sum(densities[:10]), 4)
    lte20 = round(sum(densities[:20]), 4)
    lte50 = round(sum(densities[:50]), 4)
    lte80 = round(sum(densities[:80]), 4)
    gt80 = round(sum(densities[81:101]), 4)

    cutoff = 0
    total = 0
    for i in range(len(densities)):
        total += densities[i]
        if total >= cutoff_density:
            break
        cutoff = i

    plt.hist(pops, weights=counts, range=pop_range, bins=bins, density=density)
    plt.title(collection)
    stats = "# docs = " + str(numDocs) + "\n\n" \
            + "pop(0) = " + str(pop0) \
            + " (" + str(math.floor(round(100 * pop0 / numDocs, 0))) + "%)\n" \
            + "max(pop) = " + str(max_pop_value) + "\n\n" \
            + "%.4f" % lte1 + " <= pop(1)" + "\n" \
            + "%.4f" % lte5 + " <= pop(5)" + "\n" \
            + "%.4f" % lte10 + " <= pop(10)" + "\n" \
            + "%.4f" % lte20 + " <= pop(20)" + "\n" \
            + "%.4f" % lte50 + " <= pop(50)" + "\n" \
            + "%.4f" % lte80 + " <= pop(80)" + "\n" \
            + "%.4f" % gt80 + " >  pop(80)" + "\n\n" \
            + "pop(>=" + "%.4f" % cutoff_density + ") = " + str(cutoff)

    plt.text(60, round(max_density, 1) - 0.05, stats, horizontalalignment='left', verticalalignment='top')
    plt.savefig("popdensity-revised/" + collection + "-scaled.png")
    plt.close()

# gen some java mappings, conflating 98+ to 100 since operationally everyone is capping at 98 anyway and
# we can avoid some expensive math by rounding to 100
for key in cutoffs:
    cutoff = cutoffs[key]
    if cutoff >= 98:
        cutoff = 100

    print("CUTOFFS.put(\"" + str(key) + "\", " + str(cutoff) + ");")
