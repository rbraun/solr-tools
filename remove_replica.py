from solr.collections_client import CollectionsClient
from solr.exceptions import ReplicaNotFound

target_collections = [
    'ANY-TOWN',
    'IL-SKOKIE',
    'ONT-OTTAWA'
]

patient = 'solr-stg-cld03.bcommons.net'

node_client = CollectionsClient(patient)

for collection in target_collections:
    try:
        node_client.remove_replica(collection)
        print()
    except ReplicaNotFound:
        print("Collection " + collection + " does not exist on " + patient + ".")
