import pandas as pd

from environment import Environment
from solr.network import SolrNetwork


def generate_allocations_csv(env: Environment.STAGE):
    network = SolrNetwork(env)

    dfs = []
    mems = []

    for node in network.solr_nodes:
        rows = [create_collection_row(collection, network, node) for collection in node.collections]
        rows.append([])
        rows.append(['Total', node.total_size_gb, '', '', ""])

        mems.append([node.full_name, node.esx_host, node.total_size_gb])
        dfs.append([node.digit_name,
                    pd.DataFrame(rows, columns=['Collection', 'Size GB', 'Nodes', 'Esx Hosts', 'Notes'])])

    writer = pd.ExcelWriter(f'allocations_{env.value}.xlsx', engine='xlsxwriter')
    for df in dfs:
        df[1].to_excel(writer, sheet_name=df[0])

    mem_df = pd.DataFrame(data=mems, columns=['Node', 'ESX Host', 'Size GB'])
    mem_df.to_excel(writer, sheet_name="Totals")

    writer.save()


def create_collection_row(collection, network, node):
    node_ids = [node.digit_name]
    esx_ids = [node.esx_host]
    for host in network.collections_map[collection.name]:
        if host[0] != node.digit_name:
            node_ids.append(host[0])
            esx_ids.append(host[1])

    notes = ''
    if len(set(esx_ids)) != len(esx_ids):
        notes = "ERROR: multiple replicas exist on same ESX host!"
        print(notes)

    return [collection.name, collection.size_gb, node_ids, esx_ids, notes]


if __name__ == "__main__":
    generate_allocations_csv(Environment.STAGE)
