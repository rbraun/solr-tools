from environment import Environment
from solr.collections_client import CollectionsClient
from solr.exceptions import ReplicaAlreadyExists
from solr.network import SolrNetwork
from solr.status_client import ClusterStatusClient

collections = [
    'WA-SEATTLE',
    'BC-SITKA'
]

status_client = ClusterStatusClient('slr-stg-cld01.bcommons.net')

for collection in collections:
    status = status_client.get_current_status()

    # just make sure collection is recognized
    catalog_replica_servers = [replica.server for replica in status.get_replicas_for_collection(collection)]
    if not any(catalog_replica_servers):
        raise Exception(f'No collection called {collection} exists.')

    # if replicas exist, locations must match collection locations; if so, we are done, otherwise error out
    shelf_collection = f'UGC-SHELF-{collection}'
    shelf_replicas = status.get_replicas_for_collection(shelf_collection)
    if any(shelf_replicas):
        if len(shelf_replicas) != len(catalog_replica_servers):
            raise Exception(f'Shelf count of {len(shelf_replicas)} does not match collection count '
                            f'of {len(catalog_replica_servers)}.')
        for shelf_replica in shelf_replicas:
            if not shelf_replica.server in catalog_replica_servers:
                raise Exception(f'Replica {shelf_replica} is misplaced.')
        print (f'{shelf_collection} already exists, skipping it.')
        continue

    # create the new shelf collection on the collection servers
    nodeset = ",".join((f'{server}:8983_solr' for server in catalog_replica_servers))
    node_client = CollectionsClient('slr-stg-cld02.bcommons.net')
    node_client.create_collection(shelf_collection, nodeset)
    print()
