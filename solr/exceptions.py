class ReplicaAlreadyExists(Exception):
    pass


class ReplicaNotFound(Exception):
    pass


class CollectionAlreadyExists(Exception):
    pass
