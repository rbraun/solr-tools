import urllib.request
from collections import defaultdict
from enum import Enum
from xml.etree import ElementTree as ElementTree


class ClusterStatusClient:
    def __init__(self, status_server):
        self.status_server = status_server

    def get_current_status(self):
        return ClusterStatus(self.status_server)


class Replica:
    class State(Enum):
        def __str__(self):
            return self.value

        ACTIVE = 'active'
        RECOVERING = 'recovering'
        DOWN = 'down'
        FAILED = 'recovery failed'
        GONE = 'gone'

    def __init__(self, name, collection, server, state, leader):
        self.name = name
        self.collection = collection
        self.server = server
        self.state = state
        self.leader = leader

    def __repr__(self):
        return 'Replica(%s)' % vars(self)


class ClusterStatus:
    def __init__(self, status_server):
        status_url = 'http://' + status_server + ':8983/solr/admin/collections?action=CLUSTERSTATUS'
        status_tree = ElementTree.fromstring(urllib.request.urlopen(status_url).read())

        self.live_nodes = self._parse_live_nodes(status_tree)

        self.server_replica_map = defaultdict(list)
        self.collection_replica_map = defaultdict(list)
        for replica in self._parse_replicas(status_tree):
            self.server_replica_map[replica.server].append(replica)
            self.collection_replica_map[replica.collection].append(replica)

    @staticmethod
    def _parse_live_nodes(status_tree):
        return [elem.text.split(':')[0]
                for elem in status_tree.findall("./lst[@name='cluster']/arr[@name='live_nodes']/str")]

    @staticmethod
    def _parse_replica(replica_tree):
        name = replica_tree.get('name')
        collection = replica_tree.findall("str[@name='core']")[0].text.split('_shard')[0]
        server = replica_tree.findall("str[@name='node_name']")[0].text.split(':')[0]
        state = Replica.State(replica_tree.findall("str[@name='state']")[0].text)
        leader = bool(replica_tree.findall("str[@name='leader']"))

        return Replica(name, collection, server, state, leader)

    def _parse_replicas(self, status_tree):
        return [self._parse_replica(replica_tree) for replica_tree in status_tree.findall(
                "./lst[@name='cluster']/lst[@name='collections']/lst/lst/lst/lst[@name='replicas']/*")]

    def get_live_nodes(self):
        return self.live_nodes.copy()

    def get_replicas(self, server):
        return self.server_replica_map[server].copy()

    def get_replica_for_collection_on_server(self, collection, server):
        for replica in self.collection_replica_map[collection]:
            if replica.server == server:
                return replica
        return None

    def get_replicas_for_collection(self, collection):
        return self.collection_replica_map[collection]
