from collections import defaultdict
from enum import Enum

from environment import Environment
from inventory import Inventory, ServerType
import xml.etree.ElementTree as ElementTree
import urllib.request
import re

from solr.collections_client import CollectionsClient
from solr.status_client import ClusterStatus
from vmhosts import VmHostInventory


class CollectionType(Enum):
    CATALOG = 1
    SHELF = 2
    CONTENT = 3
    LIST = 4
    USER_BIB = 5


class SolrNetwork:
    def __init__(self, env: Environment = Environment.STAGE):
        servers = Inventory(env).hosts(ServerType.SOLR_SERVERS)
        vm_hosts = VmHostInventory(env)

        # validate that the network lives nodes match the inventory nodes just for sanity
        cluster_status = ClusterStatus(servers[0])
        if set(cluster_status.get_live_nodes()) != set(servers):
            raise Exception(f'ERROR: live nodes {cluster_status.get_live_nodes()} != inventory nodes {servers}.')

        self.solr_nodes = sorted([SolrNode(server, vm_hosts.get_host(server)) for server in servers],
                                 key=lambda node: node.node_id)

        self.collections_map = defaultdict(list)

        self.collections_map = self._create_collections_map(self.solr_nodes)

    def __repr__(self):
        return "SolrNetwork(%s)" % vars(self)

    @staticmethod
    def _create_collections_map(solr_nodes):
        collections_map = {}
        for solr_node in solr_nodes:
            for collection in solr_node.collections:
                hosts = collections_map.get(collection.name, list())
                hosts.append((solr_node.digit_name, solr_node.esx_host))
                collections_map[collection.name] = hosts

        return collections_map


class SolrNode:
    def __init__(self, server, vm_host):
        self.full_name = server
        self.short_name = server.split('.')[0]

        node_id = re.search(r"\d+", self.short_name)
        self.digit_name = node_id.group(0) if node_id else None
        self.node_id = int(self.digit_name) if self.digit_name else None

        self.esx_host = vm_host.get_esx_id() if vm_host else None

        self.collections = self._summarize_collections(server)

        self.total_size_gb = round(sum([solr_node.size_gb for solr_node in self.collections]), 2)

    def __repr__(self):
        return 'SolrNode(%s)' % vars(self)

    def list(self):
        return self.collections.copy()

    def _summarize_collections(self, server):
        url = 'http://' + server + ':8983/solr/admin/cores'
        tree = ElementTree.fromstring(urllib.request.urlopen(url).read())

        collections = []
        for collection in tree.findall("./lst[@name='status']/*"):
            collection_name = re.compile('(.+)_shard.*').findall(collection.get('name'))[0]
            size = self._convert_size_description_to_gb(collection)

            collections.append(Collection(collection_name, size))

        return collections

    @staticmethod
    def _convert_size_description_to_gb(collection):
        text = collection.find("lst[@name='index']/str[@name='size']").text
        parts = text.split()
        size = float(parts[0].replace(',', ''))
        scale = {'GB': size, "MB": size / 1024, "bytes": size / 1073741824}
        return round(scale.get(parts[1]), 2)


class Collection:
    def __init__(self, name, size_gb):
        self.name = name
        self.size_gb = size_gb
        self.collection_type = self._determine_collection_type(name)

    def __repr__(self):
        return 'Collection(%s)' % vars(self)

    @staticmethod
    def _determine_collection_type(collection):
        if collection.startswith('SHELF'):
            return CollectionType.SHELF
        elif collection == 'UGC-LIST':
            return CollectionType.LIST
        elif collection == 'UGC-CONTENT':
            return CollectionType.CONTENT
        elif collection == 'UGC-BIB':
            return CollectionType.USER_BIB
        else:
            return CollectionType.CATALOG


if __name__ == "__main__":
    network = SolrNetwork()
    print(network)

    CollectionsClient()
